import os
import re
from collections import namedtuple

import math

HTMLDEC = namedtuple('HTMLDEC', ['beg', 'end'])


def get_tweet_block(data, tag, dec):
    beg = data.find(tag)
    while beg != -1:
        end = data.find(dec.end, beg)
        while end != -1:
            if data[beg: end+len(dec.end)].count(dec.beg) == data[beg: end+len(dec.end)].count(dec.end):
                yield data[beg: end+len(dec.end)]
                break
            end = data.find(dec.end, end+1)
        else:
            end = len(data)
        beg = data.find(tag, end)


def get_tweet(twidata, fromuser, touser):
    activity_type = HTMLDEC('<b class="activity-type', '>')
    if activity_type.beg not in twidata:
        author = HTMLDEC('<span class="username js-action-profile-name', '</span>')
        author = twidata[twidata.find(author.beg) + len(author.beg): twidata.find(author.end, twidata.find(author.beg))]
        assert author.count('<b>') == author.count('</b>') == 1
        author = author[author.find('<b>') + 3: author.find('</b>')].strip()

        if author == fromuser:
            message = HTMLDEC('<p class="TweetTextSize', '</p>')
            message = twidata[twidata.find('>', twidata.find(message.beg)) + 1:
                              twidata.find(message.end, twidata.find(message.beg))].lower()
            if touser in message:
                # Очищаем от мусора
                for ref in get_tweet_block(message, '<a', HTMLDEC('<a', '/a>')):
                    message = message.replace(ref, '')
                for trash in sorted(re.findall('[0-9,:%=/\.\?!\-\+\*\"\(\)]', message),
                                    key=lambda item: len(item), reverse=True):
                    message = message.replace(trash, ' ')
                return author, message
    return '', ''


def find_synonyms(words):
    while True:
        startind = 0
        for i, word1 in enumerate(words[startind:]):
            startind = i
            wlen1 = len(word1)
            for j, word2 in enumerate(words[i+1:]):
                wlen2 = len(word2)
                if wlen1 > 3 and wlen2 > 3:
                    same = 0
                    for l1, l2 in zip(word1, word2):
                        if l1 == l2:
                            same += 1
                    if same/min(wlen1, wlen2) >= 0.75:
                        print(word1, word2, round(same/min(wlen1, wlen2), 2))

                        words.pop(i) if wlen2 < wlen1 else words.pop(j)
                        break
            else:
                continue
            break
        else:
            break


def getwords(data, fromuser, touser):
    cnt = 0
    words = set()
    for tweet in get_tweet_block(data, '<li class="js-stream-item stream-item stream-item', HTMLDEC('<li', '</li>')):
        author, message = get_tweet(tweet, fromuser, touser)
        if author == fromuser and message:
            for word in message.split():
                if len(word) > 2:
                    words.add(word)
            cnt += 1
    find_synonyms(sorted(words))
    print(len(words))
    print(sorted(words))
    print(cnt)
    

if __name__ == '__main__':
    path = os.path.abspath(os.path.curdir)
    user1, user2 = 'alakhverdyants', 'prekrasnoe20161'
    with open(path+os.sep+'data'+os.sep+'me.html', encoding='utf-8') as f1:
        with open(path+os.sep+'data'+os.sep+'prekrasnoe.html', encoding='utf-8') as f2:
            getwords(f1.read(), user1, user2)
            getwords(f2.read(), user2, user1)
    print(path)
